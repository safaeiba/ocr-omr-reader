"""This module tests the packages that need to be imported for omr_extract.py module."""

def test_import():
    """Returns the result of importing the packages."""
    import cv2
    import imutils
    import matplotlib
    import numpy
    import pandas
